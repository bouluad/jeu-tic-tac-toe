//
//  ViewController.swift
//  tic-tac-toe
//
//  Created by Mohammed Bouluad on 28/09/2016.
//  Copyright © 2016 Mohammed. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    

    private var mTour : Bool? = nil
    @IBOutlet var image1 : UIImageView? = nil
    @IBOutlet var image2 : UIImageView? = nil
    @IBOutlet var image3 : UIImageView? = nil
    @IBOutlet var image4 : UIImageView? = nil
    @IBOutlet var image5 : UIImageView? = nil
    @IBOutlet var image6 : UIImageView? = nil
    @IBOutlet var image7 : UIImageView? = nil
    @IBOutlet var image8 : UIImageView? = nil
    @IBOutlet var image9 : UIImageView? = nil
    
    @IBOutlet var btn1: UIButton!
    @IBOutlet var btn2: UIButton!
    @IBOutlet var btn3: UIButton!
    @IBOutlet var btn4: UIButton!
    @IBOutlet var btn5: UIButton!
    @IBOutlet var btn6: UIButton!
    @IBOutlet var btn7: UIButton!
    @IBOutlet var btn8: UIButton!
    @IBOutlet var btn9: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

